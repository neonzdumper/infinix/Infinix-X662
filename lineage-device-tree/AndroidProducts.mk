#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_Infinix-X662.mk

COMMON_LUNCH_CHOICES := \
    lineage_Infinix-X662-user \
    lineage_Infinix-X662-userdebug \
    lineage_Infinix-X662-eng
